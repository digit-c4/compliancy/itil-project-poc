# ITIL Proof of Concept

This project intends to establish a repository structure for transversal documentation related to ITIL.

## What is the Purpose of these Best Practices?

Given that transversal projects deviate from the standard stucture established for all our services, the purpose is to organise ITIL documentation in a way that best suits its specific goals. This project assumes that readers are familiar with our general best practices for document management in gitlab which are found in the
[DocLab](https://code.europa.eu/digit-c4/compliancy/doclab)
and the
[Document management practices](https://code.europa.eu/digit-c4/compliancy/doc-management-practices).

## Repository Structure

This repository uses a specific directory structure tailored to our ITIL documentation practices. It provides a framework example, with the "Design Coordination" documents developed to illustrate the approach.
Here's an overview of this structure:

```
📦service -repo/
├── docs/ (contains subdirectories for each ITIL process family)`
│   ├── continual-service-improvement/ 
│   ├── information-management/
│   ├── project-management-framework/ 
│   ├── service-design/ (dedicated to the service design process family)
│   │   ├── availability/
│   │   ├── capacity-performance/
│   │   ├── design-coordination/ (contains deliverables related to design coordination)
│   │   │   ├── design-coordination-sac.md (outlines the service acceptance criteria for the process doc)
│   │   │   ├── design-coordination-practice.md (describes the process as validated by the customer)
│   │   │   ├── design-coordination-kpi-doc.md (describes the kpi as validated by the customer)
│   │   │   ├── design-coordination-kpi.csv (a summary table with the process KPIs)
│   │   │   ├── images/ (images, diagrams, other multimedia)
│   │   ├── it-architecture/
│   │   ├── ... (additional process categories are required)
│   ├── service-operation/
│   ├── service-transition/ 
│   ├── software-development-management/
│   ├── workforce-talent-management/
├── README.md (this file)
├── CONTRIBUTING.md (how-to contribute to this project)
```

## Feedback & Collaboration

This repository is under construction and your input is invaluable during this phase. If you have suggestions, questions, or encounter any issues, please raise them in the Issues section.


